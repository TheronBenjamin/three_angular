import { NgtSobaOrbitControls } from '@angular-three/soba/controls';
import { NgtGLTFLoaderService } from '@angular-three/soba/loaders';
import { Component, Input, OnInit } from '@angular/core';
import { MeshStandardMaterial, Object3D, Mesh, PerspectiveCamera } from 'three';

@Component({
  selector: 'app-product-preview',
  templateUrl: './product-preview.component.html',
  styleUrls: ['./product-preview.component.scss']
})
export class ProductPreviewComponent implements OnInit {
  
  @Input() 
  set color(value: string) {
    this.#color = value;
    this.applyColorToMaterial(value);
  }

  #color = '';

  cupMaterial: MeshStandardMaterial | undefined;
  cupMaterial1: MeshStandardMaterial | undefined;
  cupMaterial2: MeshStandardMaterial | undefined;
  cupMaterial3: MeshStandardMaterial | undefined;

  constructor(private gltfLoaderService: NgtGLTFLoaderService) {}

  cup$ = this.gltfLoaderService.load('assets/donut.glb');

  cupLoaded(object: Object3D) {
    console.log(object)
    object.translateX(0)
    object.translateY(-0.22)
    object.translateZ(-1)
    this.cupMaterial = <MeshStandardMaterial>(<Mesh>object.getObjectByName('donut_icing')).material
    // this.cupMaterial1 = <MeshStandardMaterial>(<Mesh>object.getObjectByName('imagetostl_mesh_2')).material
    // this.cupMaterial2 = <MeshStandardMaterial>(<Mesh>object.getObjectByName('imagetostl_mesh_3')).material
    // this.cupMaterial3 = <MeshStandardMaterial>(<Mesh>object.getObjectByName('imagetostl_mesh_4')).material

    this.applyColorToMaterial(this.#color);
  }

  ngOnInit() {
    
  }

  controlsReady(controls: NgtSobaOrbitControls) {
    const orbitControls = controls.controls;
    orbitControls.enableZoom = true;
    orbitControls.autoRotate = false;
    orbitControls.autoRotateSpeed = 10;
    const camera = orbitControls.object as PerspectiveCamera;
    camera.zoom = 70;
    camera.position.setY(4);
  }

  applyColorToMaterial(color: string) {
    if (this.cupMaterial) {
      this.cupMaterial.color.setHex(parseInt(color.substring(1), 16));
    }
    // if (this.cupMaterial1) {
    //   this.cupMaterial1.color.setHex(parseInt(color.substring(1), 16));
    // }
    // if (this.cupMaterial2) {
    //   this.cupMaterial2.color.setHex(parseInt(color.substring(1), 16));
    // }
    // if (this.cupMaterial3) {
    //   this.cupMaterial3.color.setHex(parseInt(color.substring(1), 16));
    // }
  }

}
